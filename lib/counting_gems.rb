require "counting_gems/version"

module CountingGems
  module Countable
    module ClassMethods
        def count_invocations_of(sym)
          alias_method "#{sym}_original", sym

          define_method(sym) do |*args|
            methods_count[sym] += 1
            send "#{sym}_original", *args
          end
        end
    end

    def self.included(includer)
      includer.extend ClassMethods
    end

    def invoked?(sym)
      methods_count[sym] > 0
    end

    def invoked(sym)
      methods_count[sym]
    end

    def methods_count
      @methods_count ||= Hash.new(0)
    end
  end
end
